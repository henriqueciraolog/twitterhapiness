package br.com.hciraolo.twitterhapiness.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Text {

    @SerializedName("content")
    @Expose
    var content: String? = null
    @SerializedName("beginOffset")
    @Expose
    var beginOffset: Int? = null

}