package br.com.hciraolo.twitterhapiness.twitter

import br.com.hciraolo.twitterhapiness.service.TweetService
import com.twitter.sdk.android.core.models.Tweet
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable


internal class RxTwitterApiClient() : TwitterApiClient() {

    /**
     * Provide CustomService with defined endpoints
     */
    val tweetService: TweetService
        get() = getService(TweetService::class.java)
}