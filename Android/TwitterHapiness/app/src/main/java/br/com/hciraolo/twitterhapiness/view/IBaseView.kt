package br.com.hciraolo.testeenjoei.core.base

import android.app.Activity
import android.content.Context

interface IBaseView {
    val context: Context
    val activity: Activity
    fun isOnline() : Boolean
    fun showProgress()
    fun hideProgress()
}
