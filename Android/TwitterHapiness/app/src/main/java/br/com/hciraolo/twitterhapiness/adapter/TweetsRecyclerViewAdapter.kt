package br.com.hciraolo.twitterhapiness.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.hciraolo.twitterhapiness.R
import br.com.hciraolo.twitterhapiness.viewholder.TweetViewHolder
import br.com.hciraolo.twitterhapiness.model.TweetHapinessModel

class TweetsRecyclerViewAdapter() : RecyclerView.Adapter<TweetViewHolder>() {
    private var mDataset: MutableList<TweetHapinessModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TweetViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.adapter_view_tweet, parent, false)
        return TweetViewHolder(v)
    }

    override fun onBindViewHolder(holder: TweetViewHolder, position: Int) {
        val product = mDataset[position]
        holder.onBind(product, (position + 1 == mDataset.size))
    }

    override fun getItemCount(): Int {
        return mDataset.size
    }

    fun addItems(database: MutableList<TweetHapinessModel>) {
        mDataset.addAll(database)
        notifyDataSetChanged()
    }

    fun clearList() {
        mDataset.clear()
        notifyDataSetChanged()
    }
}