package br.com.hciraolo.testeenjoei.core.base

import android.app.Activity
import android.content.Context

import androidx.appcompat.app.AppCompatActivity
import br.com.hciraolo.twitterhapiness.utils.NetworkStatsUtil
import br.com.hciraolo.twitterhapiness.dialog.ProgressBottomSheetDialog

abstract class BaseActivity : AppCompatActivity(), IBaseView {

    override val context: Context
        get() = this

    override val activity: Activity
        get() = this

    private var progressBottomSheetDialog: ProgressBottomSheetDialog? = null

    override fun showProgress() {
        progressBottomSheetDialog = ProgressBottomSheetDialog()
        progressBottomSheetDialog!!.show(supportFragmentManager, "ProgressBottomSheetDialog")
    }

    override fun hideProgress() {
        if (progressBottomSheetDialog != null) {
            progressBottomSheetDialog!!.dismiss()
        }
    }

    override fun isOnline() : Boolean {
        return NetworkStatsUtil.isConnected(this)
    }
}
