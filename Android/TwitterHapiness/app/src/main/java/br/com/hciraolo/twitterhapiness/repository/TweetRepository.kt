package br.com.hciraolo.twitterhapiness.repository

import br.com.hciraolo.twitterhapiness.BuildConfig
import br.com.hciraolo.twitterhapiness.service.LanguageNaturalService
import br.com.hciraolo.twitterhapiness.model.RequestAnalyzeSentiment
import br.com.hciraolo.twitterhapiness.model.Document
import br.com.hciraolo.twitterhapiness.model.TweetHapinessModel
import br.com.hciraolo.twitterhapiness.service.TweetService
import com.twitter.sdk.android.core.models.Tweet
import rx.Observable
import rx.schedulers.Schedulers

class TweetRepository(private val tweetService: TweetService, private val languageNaturalService: LanguageNaturalService) {
    fun getTweets(username: String, lastTweetId: Long?) = tweetService.userTimeline(null, username, 15, null, lastTweetId, null, null, null, true)

    fun postAnalyzeSentiment(request: RequestAnalyzeSentiment) = languageNaturalService.postAnalyzeSentiment(BuildConfig.GOOGLE_KEY, request)

    fun getTweetHapiness(username: String, lastTweetId: Long?) =
            getTweets(username, lastTweetId)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(Schedulers.newThread())
                    .flatMap { t: List<Tweet> -> Observable.from(t) }
                    .flatMap { t ->
                        postAnalyzeSentiment(RequestAnalyzeSentiment(Document(language = (if (t.lang.equals("und")) "pt" else t.lang), content = t.text)))
                                .onErrorResumeNext(Observable.empty())
                                .concatMap { ras -> Observable.just(TweetHapinessModel(t, ras.documentSentiment!!)) }
                    }
                    .toList()
}