package br.com.hciraolo.twitterhapiness.model

import br.com.hciraolo.twitterhapiness.model.Document
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class RequestAnalyzeSentiment (
        @SerializedName("document")
    @Expose val document: Document,
        @SerializedName("encodingType")
    @Expose
    val encodingType: String = "UTF8"
)