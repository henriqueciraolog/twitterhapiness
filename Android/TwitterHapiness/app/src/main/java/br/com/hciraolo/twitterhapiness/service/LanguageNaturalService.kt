package br.com.hciraolo.twitterhapiness.service

import br.com.hciraolo.twitterhapiness.model.RequestAnalyzeSentiment
import br.com.hciraolo.twitterhapiness.model.ResponseAnalyzeSentiment
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query
import rx.Observable

interface LanguageNaturalService {
    @POST("v1/documents:analyzeSentiment")
    fun postAnalyzeSentiment(@Query("key") key: String, @Body request: RequestAnalyzeSentiment) : Observable<ResponseAnalyzeSentiment>
}