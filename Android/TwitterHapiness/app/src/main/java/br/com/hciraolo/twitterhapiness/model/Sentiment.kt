package br.com.hciraolo.twitterhapiness.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Sentiment {

    @SerializedName("magnitude")
    @Expose
    var magnitude: Double? = null
    @SerializedName("score")
    @Expose
    var score: Double? = null

}