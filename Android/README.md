# Humor do Twitter

O projeto em questão, teve uso, principalmente, dos frameworks Retrofit, RxJava e Twitter Kit Android. Foi utilizado Kotlin como linguagem.
Como arquitetura, foi utilizado o MVP: Model View Presenter
Para o funcionamento do Twitter Kit Android com RxJava, foi importada a classe TwitterApiClient para o projeto. Conforme documentação do Twitter Kit Android, a classe TwitterApiClient foi herdada para inclusão do endpoint com retorno em Observable.

Para a analise de sentimentos do Twitter, considerei a linguagem identificada pelo Twitter e, caso a linguagem não fosse suportada pela API, o tweet em questão não é exibido na tela.

A lista é infinita, ou seja, após um determinado momento do scroll, ele começa a carregar mais tweets.

É feita verificações de campo de branco e ausência de conexão de internet.

Honestamente, eu não fiz testes, porque eu nunca aprendi testes unitários na prática. Idem testes de UI.
