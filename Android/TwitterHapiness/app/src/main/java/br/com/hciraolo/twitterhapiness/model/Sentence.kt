package br.com.hciraolo.twitterhapiness.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Sentence {

    @SerializedName("text")
    @Expose
    var text: Text? = null
    @SerializedName("sentiment")
    @Expose
    var sentiment: Sentiment? = null

}