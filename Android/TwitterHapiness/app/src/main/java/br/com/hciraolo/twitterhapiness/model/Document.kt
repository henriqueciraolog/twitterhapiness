package br.com.hciraolo.twitterhapiness.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class Document (

    @SerializedName("type")
    @Expose
    val type: String = "PLAIN_TEXT",
    @SerializedName("language")
    @Expose
    val language: String,
    @SerializedName("content")
    @Expose
    val content: String

)