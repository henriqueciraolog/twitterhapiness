package br.com.hciraolo.twitterhapiness.model

import br.com.hciraolo.twitterhapiness.model.Sentence
import br.com.hciraolo.twitterhapiness.model.Sentiment
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ResponseAnalyzeSentiment {

    @SerializedName("documentSentiment")
    @Expose
    var documentSentiment: Sentiment? = null
    @SerializedName("language")
    @Expose
    var language: String? = null
    @SerializedName("sentences")
    @Expose
    var sentences: List<Sentence>? = null

}