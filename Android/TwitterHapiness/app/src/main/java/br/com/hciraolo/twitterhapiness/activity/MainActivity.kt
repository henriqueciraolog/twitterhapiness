package br.com.hciraolo.twitterhapiness.activity

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.hciraolo.testeenjoei.core.base.BaseActivity
import br.com.hciraolo.twitterhapiness.listener.EndlessRecyclerViewScrollListener
import br.com.hciraolo.twitterhapiness.presenter.MainPresenter
import br.com.hciraolo.twitterhapiness.R
import br.com.hciraolo.twitterhapiness.model.TweetHapinessModel
import br.com.hciraolo.twitterhapiness.adapter.TweetsRecyclerViewAdapter
import br.com.hciraolo.twitterhapiness.view.IMainView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), IMainView, View.OnClickListener {

    private var hasMoreTweets = false
    private lateinit var mPresenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        MainPresenter(this)
        mPresenter.start()

    }

    override fun setPresenter(presenter: MainPresenter) {
        this.mPresenter = presenter
    }

    override fun initViews() {
        btnSearchTweets.setOnClickListener(this)

        val dividerItemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        rcvTweets.addItemDecoration(dividerItemDecoration)

        rcvTweets.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rcvTweets.setHasFixedSize(false)
        rcvTweets.addOnScrollListener(object : EndlessRecyclerViewScrollListener(rcvTweets.layoutManager as LinearLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                if (hasMoreTweets) mPresenter.loadTweets(false)
            }
        })
        rcvTweets.adapter = TweetsRecyclerViewAdapter()

        txtTwitterUser.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (rcvTweets.adapter!!.itemCount > 0) {
                    (rcvTweets.adapter as TweetsRecyclerViewAdapter).clearList()
                    mPresenter.twitterUsername = null
                }

                if (cnlErrorView.visibility == View.VISIBLE) {
                    rcvTweets.visibility = View.VISIBLE
                    cnlErrorView.visibility = View.GONE
                }
            }
        })
        val d = ContextCompat.getDrawable(this, R.drawable.ic_search_white_24dp)
        txtTwitterUser.setCompoundDrawablesWithIntrinsicBounds(d, null, null, null)
    }

    override fun loadItems(tweets: List<TweetHapinessModel>) {
        rcvTweets.visibility = View.VISIBLE
        cnlErrorView.visibility = View.GONE

        hasMoreTweets = true
        (rcvTweets.adapter as TweetsRecyclerViewAdapter).addItems(tweets.toMutableList())
    }

    override fun errorOnLoadItems(throwable: Throwable) {
        hasMoreTweets = false
        if ((rcvTweets.adapter as TweetsRecyclerViewAdapter).itemCount == 0) {
            rcvTweets.visibility = View.GONE
            cnlErrorView.visibility = View.VISIBLE

            txvWarningText.text = getString(R.string.str_error_unknown)
        }
    }

    override fun errorWithoutConnection() {
        rcvTweets.visibility = View.GONE
        cnlErrorView.visibility = View.VISIBLE

        txvWarningText.text = getString(R.string.str_error_nonconnection)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnSearchTweets -> {
                val twitterUsername = txtTwitterUser.text.toString()
                if (!twitterUsername.equals("")) {
                    mPresenter.twitterUsername = twitterUsername
                    mPresenter.loadTweets(true)
                } else {
                    tilTwitterUser.error = getString(R.string.str_error_twitter_username_blank)
                }
            }
        }
    }
}
