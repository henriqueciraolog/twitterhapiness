package br.com.hciraolo.twitterhapiness.service

import android.content.Context
import android.util.Log
import br.com.hciraolo.twitterhapiness.BuildConfig
import br.com.hciraolo.twitterhapiness.twitter.RxTwitterApiClient
import com.google.gson.GsonBuilder
import com.twitter.sdk.android.core.DefaultLogger
import com.twitter.sdk.android.core.Twitter
import com.twitter.sdk.android.core.TwitterAuthConfig
import com.twitter.sdk.android.core.TwitterConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ServicesFactory {


    fun getTwitterService() : TweetService {
        val twitterApiClient = RxTwitterApiClient()
        return twitterApiClient.tweetService
    }

    fun getLanguageNaturalService() : LanguageNaturalService {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY


        val client = OkHttpClient.Builder()
                .connectTimeout(45, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(45, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build()


        val gson = GsonBuilder().create()

        val retroBuilder = Retrofit.Builder()
                .baseUrl(BuildConfig.GOOGLE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)

        return retroBuilder.build().create<LanguageNaturalService>(LanguageNaturalService::class.java)
    }
}