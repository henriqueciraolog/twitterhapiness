package br.com.hciraolo.twitterhapiness.model

import com.twitter.sdk.android.core.models.Tweet

data class TweetHapinessModel (val tweet: Tweet, val sentiment: Sentiment)