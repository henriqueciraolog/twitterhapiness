package br.com.hciraolo.twitterhapiness.presenter

import br.com.hciraolo.testeenjoei.core.base.IBasePresenter
import br.com.hciraolo.twitterhapiness.service.ServicesFactory
import br.com.hciraolo.twitterhapiness.model.TweetHapinessModel
import br.com.hciraolo.twitterhapiness.repository.TweetRepository
import br.com.hciraolo.twitterhapiness.service.LanguageNaturalService
import br.com.hciraolo.twitterhapiness.service.TweetService
import br.com.hciraolo.twitterhapiness.view.IMainView
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class MainPresenter(private val mView: IMainView) : IBasePresenter {

    private lateinit var tweetService: TweetService
    private lateinit var languageNaturalService: LanguageNaturalService

    var twitterUsername: String? = null
    set(value) {
        lastTweetId = null
        field = value
    }
    private var lastTweetId: Long? = null

    init {
        mView.setPresenter(this)
        tweetService = ServicesFactory.getTwitterService()
        languageNaturalService = ServicesFactory.getLanguageNaturalService()
    }

    override fun start() {
        mView.initViews()
    }

    fun loadTweets(withProgress: Boolean) {
        if (withProgress) {
            if (!mView.isOnline()) {
                mView.errorWithoutConnection()
                return
            }
            mView.showProgress()
        }

        val call = TweetRepository(tweetService, languageNaturalService).getTweetHapiness(twitterUsername!!, lastTweetId)
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Subscriber<List<TweetHapinessModel>>() {
                    override fun onCompleted() {

                    }

                    override fun onError(e: Throwable) {
                        mView.hideProgress()
                        e.printStackTrace()
                        mView.errorOnLoadItems(e)
                    }

                    override fun onNext(response: List<TweetHapinessModel>) {
                        mView.hideProgress()
                        lastTweetId = response[response.size - 1].tweet.id - 1
                        mView.loadItems(response)
                    }
                })
    }
}