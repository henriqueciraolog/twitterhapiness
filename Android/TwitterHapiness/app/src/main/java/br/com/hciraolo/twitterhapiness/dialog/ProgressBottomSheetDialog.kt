package br.com.hciraolo.twitterhapiness.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.hciraolo.twitterhapiness.R

import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet_dialog_progress.*
import kotlinx.android.synthetic.main.bottom_sheet_dialog_progress.view.*

class ProgressBottomSheetDialog : BottomSheetDialogFragment() {

    private var loadingText: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.bottom_sheet_dialog_progress, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (loadingText != null) view.txvText.text = loadingText

        isCancelable = false
    }

    fun setLoadingText(loadingText: String) {
        this.loadingText = loadingText
        if (txvText != null) txvText.setText(loadingText)
    }
}
