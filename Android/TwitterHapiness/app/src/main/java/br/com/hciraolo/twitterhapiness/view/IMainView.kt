package br.com.hciraolo.twitterhapiness.view

import br.com.hciraolo.testeenjoei.core.base.IBaseView
import br.com.hciraolo.twitterhapiness.model.TweetHapinessModel
import br.com.hciraolo.twitterhapiness.presenter.MainPresenter


interface IMainView : IBaseView {
    fun setPresenter(presenter: MainPresenter)
    fun initViews()
    fun loadItems(products: List<TweetHapinessModel>)
    fun errorOnLoadItems(throwable: Throwable)
    fun errorWithoutConnection()
}