package br.com.hciraolo.twitterhapiness.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.com.hciraolo.twitterhapiness.model.TweetHapinessModel
import kotlinx.android.synthetic.main.adapter_view_tweet.view.*


class TweetViewHolder internal constructor(v: View) : RecyclerView.ViewHolder(v) {
    fun onBind(tweetHapiness: TweetHapinessModel, lastItem: Boolean) {
        itemView.txvTweetText.text = tweetHapiness.tweet.text

        itemView.txvTweetHapiness.text = returnHapiness(tweetHapiness.sentiment.score!!)

        if (lastItem) itemView.pbrLoadMore.visibility = View.VISIBLE
        else itemView.pbrLoadMore.visibility = View.GONE
    }

    fun returnHapiness(score: Double) : String {
        if (score > 0.25) {
            return "\uD83D\uDE0A"
        } else if (score <= 0.25 && score > -0.25) {
            return "\uD83D\uDE10"
        } else {
            return "\uD83D\uDE14"
        }
    }
}